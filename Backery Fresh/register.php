<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register | Bakery Fresh</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i>
                        Backery.fresh@gmail.com
                       
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i>
                        +216 22.188.888
                    </div>
                </div>
                <div class="ht-right">
                    <a href="login.php" class="login-panel" ><i class="fa fa-user"></i>Connexion </a>
                    <a href="register.php" class="login-panel" ><i class="fa fa-user"></i>s'inscrire</a>
                    <a href="panier.php" class="login-panel"><i class="fa fa-shopping-cart"></i>panier</a>
                    <div class="top-social">
                        <a href="#"><i class="ti-facebook"></i></a>
                        <a href="#"><i class="ti-twitter-alt"></i></a>
                        <a href="#"><i class="ti-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="inner-header">
                <div class="row">
                    <div class="col-lg-9 col-md-9">
                        <div class="logo">
                                <img  src="logo.png"  style ="margin-left:350px; margin-bottom: 250px;" alt="Backery Fresh">                          
                                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
            <div class="container">
                <nav class="nav-menu mobile-menu">
                    <ul>
                        <li class="active"><a href="./index.html">Accueil</a></li>
                        <li><a href="apropos.html">A propos</a></li>
                        <li><a href="produit.php">Produits</a>
                            <ul class="dropdown">
                                <li><a href="produit.php">sucré</a></li>
                                <li><a href="produit.php">Salé</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="./blog.html">Blog</a></li>
                       

                      <!--  <li><a href="./contact1.html">contact</a></li>-->
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="#"><i class="fa fa-home"></i> Accueil</a>
                        <span>S'inscrire</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Form Section Begin -->

    <!-- Register Section Begin -->
    <div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="register-form">
                        <h2>inscription</h2>
                        <form action="inscription.php" method="POST">
                            <div class="group-input">
                                <label for="username">nom</label>
                                <input type="text" name="nom" required="required">
                            </div>
                            <div class="group-input">
                                <label for="pass">email</label>
                                <input type="email" name="email" required="required">
                            </div>
                            <div class="group-input">
                                <label for="con-pass"> mot de passe</label>
                                <input type="password" name="password"required="required">
                            </div>
                            <button type="submit"  name='submit'class="site-btn register-btn"required="required" href="login.php">inscription</button>
                        </form>
                        <div class="switch-login">
                            <a href="./login.php" class="or-login"required="required">Ou Connexion</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->
    <!-- Partner Logo Section Begin -->
    <!-- Partner Logo Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <a href="#"><img src="logo.png" alt=""></a>
                        </div>
                        <ul  style="margin-top:30px;">
                            <li>Address: 60-49 nabeul 11378 Tunisie</li>
                            <li>Téléphone: +216 22.188.888</li>
                            <li>Email: Backery.fresh@gmail.com</li>
                        </ul>
                        <div class="footer-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1">
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-4"  >
                    <div class="newslatter-item" >
                        <h5>Rejoignez notre newsletter maintenant</h5>
                        <p>Recevez des mises à jour par e-mail sur notre dernière boutique et nos offres spéciales. </p>
                        <form action="#" class="subscribe-form">
                            <input type="email" placeholder="Entrer votre Email">
                            <button type="button">S'abonner </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                             Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tous les droits sont réservés  | Ce modèle  <i class="fa fa-heart-o" aria-hidden="true"></i>est fait par <a href="https://colorlib.com" target="_blank">Colorlib</a>
                             <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <div class="payment-pic">
                            <img src="img/payment-method.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.zoom.min.js"></script>
    <script src="js/jquery.dd.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>