-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 23 fév. 2021 à 18:31
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `produits`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` int(11) NOT NULL,
  `inscription_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `total`, `inscription_id`) VALUES
(37, 50, 5),
(38, 35, 5),
(39, 35, 6),
(40, 100, 6),
(41, 70, 5);

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

DROP TABLE IF EXISTS `inscription`;
CREATE TABLE IF NOT EXISTS `inscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`id`, `nom`, `email`, `password`) VALUES
(5, 'saffar soumaya', 'saffar.soumaya@gmail.com', 'test'),
(6, 'rahma', 'asma@yahoo.fr', 'ee');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prix` float NOT NULL DEFAULT 0,
  `img` text NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `prix`, `img`, `type`) VALUES
(1, 'Gateau etage aux framboises', 50, 'Gateau etage aux framboises.jpg', 'sucre'),
(2, 'Tarte au chocolat blanc', 35, 'Tarte au chocolat blanc.jpg', 'sucre'),
(3, 'cheesecake framboise', 40, 'cheesecake framboise.jpg', 'sucre'),
(4, 'biscuit', 20, 'biscuit.jpg', 'sucre'),
(5, 'maracorn', 70, 'macaron.jpg', 'sucre'),
(6, 'crumble cake', 25, 'crumble cake.jpg', 'sucre'),
(7, 'Cookies', 20, 'COOKIES.jpg', 'sucre'),
(8, 'Cupcake', 20, 'CUPCAKE.jpg', 'sucre'),
(9, 'Eclair', 20, 'ECLAIR.jpg', 'sucre'),
(10, 'Feuilletés au saumon fumé', 8, 'FEUILLETÉS AU SAUMON FUMÉ.jpg', 'sale'),
(11, 'Croissant salé tomate-mozzarella', 6, 'CROISSANT SALÉ TOMATE-MOZZARELLA.jpg', 'sale'),
(12, 'Mini pizza reine', 4, 'MINI PIZZA REINE.jpg', 'sale'),
(13, 'Roulé fromage-Lardon', 5, 'ROULÉ FROMAGE-LARDON.jpg', 'sale'),
(14, 'petits patés', 5, 'PETITS PATÉS.jpg', 'sale');

-- --------------------------------------------------------

--
-- Structure de la table `produit_commande`
--

DROP TABLE IF EXISTS `produit_commande`;
CREATE TABLE IF NOT EXISTS `produit_commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produit_id` int(11) NOT NULL,
  `commande_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit_commande`
--

INSERT INTO `produit_commande` (`id`, `produit_id`, `commande_id`, `quantite`) VALUES
(59, 2, 30, 0),
(58, 2, 29, 0),
(57, 2, 28, 0),
(56, 3, 27, 0),
(55, 2, 27, 0),
(54, 2, 26, 0),
(53, 2, 25, 0),
(60, 2, 35, 2),
(61, 3, 36, 1),
(62, 4, 36, 4),
(63, 1, 36, 2),
(64, 1, 36, 1),
(65, 2, 36, 1),
(66, 2, 36, 1),
(67, 2, 36, 1),
(68, 3, 36, 1),
(69, 1, 36, 1),
(70, 2, 36, 1),
(71, 2, 36, 1),
(72, 1, 37, 1),
(73, 2, 38, 1),
(74, 1, 38, 1),
(75, 2, 39, 1),
(76, 1, 39, 2),
(77, 1, 40, 2),
(78, 2, 41, 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
