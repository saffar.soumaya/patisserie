<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nos produits | Bakery Fresh</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<?php require "navbar.php";?>
<div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="#"><i class="fa fa-home"></i>Accueil</a>
                        <span> produits  <?php if (isset($_GET['type']))
                        {
                            $type= $_GET['type'];
                             echo(">".$type);
                        }?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<section class="product-shop spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 order-1 order-lg-12">
                
                    <div class="product-list">
                        <div class="row">
                        <?php if (isset($_GET['type']))
                        {
                            $type= $_GET['type'];
                            $produits=$DB->query("SELECT * FROM produits where type='$type'");
                      
                        }else{
                            $produits=$DB->query('SELECT * FROM `produits`');
                    
                        }
                        foreach ($produits as $produit):
                        ?>
                            <div class="col-lg-4 col-sm-4">
                              <div class="product-item">
                                    <div class="pi-pic">
                                        <img src="../admin/img/<?= $produit->img ; ?>" style="height:300px " alt="<?= $produit -> nom;?>">
                                        <div class="sale pp-sale"><?= $produit->type ; ?></div>
                                        <div class="icon">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul> 
                                            <li class="w-icon active"><a  class="addpanier" href="addpanier.php?id=<?=$produit->id;?>"><i class="icon_bag_alt"></i></a></li>
                                            <li class="quick-view"><a href="#">+ Aperçu rapide</a></li>
                                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name"><?= $produit -> nom;?></div>
                                        <div class="product-price">
                                        <?= number_format($produit -> prix,3);?>DT
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                            <?php endforeach?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  
    <!-- Footer Section Begin -->
    <?php require"footer.php";?>
    <!-- Footer Section End -->
     <!-- Js Plugins -->
    
</body>
<script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.zoom.min.js"></script>
    <script src="js/jquery.dd.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</html>