<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Panier | Bakery Fresh</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<?php require 'navbar.php';?>

<?php
 if(isset($_GET['del'])){
    $panier->del($_GET['del']);
}
?>
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <!-- Header End -->

    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="./home.html"><i class="fa fa-home"></i>Accueil</a>
                        <a href="./shop.html">Panier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form method="POST" action='panier.php'>
                    <div class="cart-table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th class="p-name">Nom du produit </th>
                                    <th>Prix </th>
                                    <th>Quantité </th>
                                    <th><i class="ti-close"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $ids=array_keys($_SESSION['panier']);
                        
                            if(empty($ids)){
                            
                                $produits =array();
                            }else{
                                $produits=$DB->query('SELECT * FROM produits WHERE id IN ('.implode(',',$ids).')');
                            }
                              foreach ($produits as $produit): ?>
                                <tr>
                                    <td class="cart-pic first-row"><img src="../admin/img/<?= $produit->img ; ?>" alt=""></td>
                                    <td class="cart-title first-row"name = 'nom'>
                                     <?= $produit->nom;?>
                                    </td>
                                    <td class="p-price first-row"> <?= number_format($produit -> prix,3);?>DT
                                    <td class="qua-col">
                                        <div class="quantity">
                                            <div class="pro-qty" name="quantite">
                                               <input  type="numbre" name='panier[quantite][<?= $produit->id;?>]' value='<?=$_SESSION['panier'][$produit->id];?>'>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="close-td first-row"> <a href="panier.php?del=<?=$produit->id;?>"><i class="ti-close"></i></a></td>
                                </tr>
                                <?php endforeach?>
                               
                            </tbody>
                        </table>
                        <div class="proceed-checkout">
                                <ul>
                                    <li class="cart-total" name ='total'>total <input type='submit'  value='Calcul'><span><?= number_format($panier->total());?></span></li>
                                </ul>    
                    </div>
                    </form>
                    <form method='POST' action='<?= $panier->addCommande() ?>' style="margin: 10px 0px;">
                    <input  class="primary-btn continue-shop" type="submit" name="ajout_commande" value="envoye" >
                    </form>     
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="cart-buttons">
                                <a href="produit.php" class="primary-btn continue-shop">Continue shopping</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->


<?php require"footer.php";?>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.zoom.min.js"></script>
    <script src="js/jquery.dd.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>