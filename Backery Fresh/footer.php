<footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <a href="index.html"><img src="logo.png" alt="Backery Fresh"></a>
                        </div>
                        <ul  style="margin-top:30px;">
                            <li>Address: 60-49 nabeul 11378 Tunisie</li>
                            <li>Téléphone: +216 22.188.888</li>
                            <li>Email: Backery.fresh@gmail.com</li>
                        </ul>
                        <div class="footer-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1">
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-4"  >
                    <div class="newslatter-item" >
                        <h5>Rejoignez notre newsletter maintenant</h5>
                        <p>Recevez des mises à jour par e-mail sur notre dernière boutique et nos offres spéciales. </p>
                        <form action="#" class="subscribe-form">
                            <input type="email" placeholder="Entrer votre Email">
                            <button type="button">S'abonner </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                             Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tous les droits sont réservés  | Ce modèle  <i class="fa fa-heart-o" aria-hidden="true"></i>est fait par <a href="https://colorlib.com" target="_blank">Colorlib</a>
                             <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <div class="payment-pic">
                            <img src="img/payment-method.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>