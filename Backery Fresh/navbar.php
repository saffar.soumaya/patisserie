<?php 
require '_navbar.php';
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
}else{
   $user = null;
}
?>
  <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i>
                        Backery.fresh@gmail.com
                       
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i>
                        +216 22.188.888
                    </div>
                </div>
                <div class="ht-right">
                    <?php if (empty($user)){?>
                    <a href="login.php" class="login-panel" ><i class="fa fa-user"></i>Connexion</a>
                    <?php } else {?>
                        <a href="deconnexion.php" class="login-panel"  ><i class="fa fa-user"></i>Déconnexion <?= $user -> nom;?>  </a>
                        <?php } ?>
                    <a href="register.php" class="login-panel" ><i class="fa fa-user"></i>s'inscrire</a>
                    <a href="panier.php" class="login-panel"><i class="fa fa-shopping-cart"></i>panier</a>
                    <div class="top-social">
                        <a href="#"><i class="ti-facebook"></i></a>
                        <a href="#"><i class="ti-twitter-alt"></i></a>
                        <a href="#"><i class="ti-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="inner-header">
                <div class="row">
                    <div class="col-lg-9 col-md-9">
                        <div class="logo">
                                <img  src="logo.png"  style ="margin-left:350px; margin-bottom: 250px;" alt="Backery Fresh">                          
                                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
            <div class="container">
                <nav class="nav-menu mobile-menu">
                    <ul>
                        <li class="active"><a href="./index.html">Accueil</a></li>
                        <li><a href="apropos.html">A propos</a></li>
                        <li><a href="produit.php">Produits</a>
                            <ul class="dropdown">
                                <li><a href="produit.php?type=sucre">sucré</a></li>
                                <li><a href="produit.php?type=sale">Salé</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="./blog.html">Blog</a></li>
                       

                      <!--  <li><a href="./contact1.html">contact</a></li>-->
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>

           